Readme File


download git program for windows
https://gitforwindows.org/

เรียก BASH Windows จากโปรแกรมที่ download มา



// ตรวจสอบว่า git ถูก login โดยใครไว้ก่อนหน้าเรา
git config --get-all user.name
หรือ
git config --get-all user.email

// เปลี่ยน email มาเป็นของเรา
git config --global user.email "donutx@gmail.com"

git config --global user.name "donutx"


// create ssh key
ssh-keygen -t rsa -C "donutx@gmail.com" -b 4096 
// Enter 3 times


// clone project
cd d:
mkdir git
cd git
git clone https://gitlab.com/donutx/testproject.git

